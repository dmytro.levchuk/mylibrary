import React, {Component} from 'react';
import {connect} from 'react-redux'
import {OPEN_ADD_NEW_WINDOW, OPEN_SIGNIN_FORM} from "../../../actions/modals";

import {
    getCabinetAuthorList, getContentAuthorList
} from "../../../actions/content";

import ItemCard from "../../ItemCard"

import AddNewForm from "../../Modals/AddNew"


class Content extends Component {

    handleAddNewForm = (e) => {

        if (e.target.dataset.btn !== 'btn-addnew-close') {
            e.stopPropagation();
        }

    }

    componentDidMount() {

        let authorName = this.props.loginInfo.loggedData.name;

        // console.log("authorName", authorName)

        this.props.getCabinetAuthorList({authorName})

    }

    render () {


        let authorListItems = []

        if (this.props.listCabinet.length > 0) {
            authorListItems = this.props.listCabinet.map((elem) => {

                return (
                    <ItemCard
                        itemUrl={elem.itemUrl}
                        type={elem.type}
                        authorName={elem.authorName}
                        title={elem.title}
                        description={elem.description}
                        rate={elem.rate}
                        imageUrl={elem.imageUrl}
                        key={elem.itemUrl}
                    />
                )


            });
        }

        return (
            <div className="content">
                <div className="content__block">
                    <input type="button" value="Add New"
                           className="sign-form__btn"
                           onClick={() => this.props.openAddNewForm()}
                    />

                    <div className="content_list">
                        {authorListItems}

                    </div>

                </div>


                <div id="content__modal">

                    {this.props.windowsStatus.formAddNewOpen && (
                        <div className="modal__background">
                            <div onClick={this.handleAddNewForm}>
                                <AddNewForm/>
                            </div>
                        </div>

                    )}

                </div>

            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        windowsStatus: state.modals.windowsStatus,
        loginInfo: state.users,
        listCabinet: state.content.listCabinet,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getCabinetAuthorList: (query) => dispatch(getCabinetAuthorList(query)),
        openAddNewForm: () => dispatch({type: OPEN_ADD_NEW_WINDOW})
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Content)