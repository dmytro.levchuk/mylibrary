import React, {Component} from 'react';
import {connect} from "react-redux";
import {Switch, Route} from "react-router-dom";
// import {Switch, Route} from "react-router";
// import './App.css';

// import Header from "./components/Header";
// import MainPage from "./components/MainPage";
// import Footer from "./components/Footer";
// import Cabinet from "./components/Cabinet";
import Dashboard from "./components/Dashboard"


import {
    CLOSE_SIGNIN_FORM, CLOSE_REGISTRATION_FORM, CLOSE_REGISTRATION_OK, CLOSE_PROFILE_WINDOW, CLOSE_ADD_NEW_WINDOW
} from "./actions/modals";

import {checkUserOnStart} from "./actions/users";

// import "./scss/style.scss";


class App extends Component {

    componentDidMount () {
        this.props.checkUserOnStart();
    }

    handleCloseForms = () => {
        if (this.props.windowsStatus.formSignInOpen) {
            this.props.closeSignInForm();
            document.body.style.overflow="auto";
        }

        if (this.props.windowsStatus.formRegistrationOpen) {
            this.props.closeRegistrationForm();
            document.body.style.overflow="auto";
        }

        if (this.props.windowsStatus.registrationOk) {
            this.props.closeRegistrationOk();
            document.body.style.overflow="auto";
        }

        if (this.props.windowsStatus.profileWindow) {
            this.props.closeProfileWindow();
            // document.body.style.overflow="auto";
        }

        if (this.props.windowsStatus.formAddNewOpen) {
            this.props.closeAddNewForm();
            // document.body.style.overflow="auto";
        }

    };


    render () {


        return (
            <div className="App" onClick={this.handleCloseForms}>
                {/*<Header/>*/}

                <Switch>
                    {/*<Route path="/cabinet" component={Cabinet}/>*/}
                    <Route path="/" component={Dashboard}/>
                </Switch>

                {/*<Footer/>*/}

            </div>
        )

    }

}

const mapStateToProps = state => {
    return {
        windowsStatus: state.modals.windowsStatus,
        loginInfo: state.users,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        checkUserOnStart: () => dispatch(checkUserOnStart()),

        closeSignInForm: () => {
            dispatch({type: CLOSE_SIGNIN_FORM});
        },
        closeRegistrationForm: () => {
            dispatch({type: CLOSE_REGISTRATION_FORM});
        },
        closeRegistrationOk: () => {
            dispatch({type: CLOSE_REGISTRATION_OK});
        },
        closeProfileWindow: () => {
            dispatch({type: CLOSE_PROFILE_WINDOW})
        },
        closeAddNewForm: () => {
            dispatch({type: CLOSE_ADD_NEW_WINDOW})
        },
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
