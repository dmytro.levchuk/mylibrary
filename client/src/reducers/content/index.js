import { PUT_LIST_IN_REDUX, PUT_ITEM_IN_REDUX, PUT_LIST_AUDIO_IN_REDUX, PUT_LIST_VIDEO_IN_REDUX,
    PUT_LIST_TEXT_IN_REDUX, PUT_LIST_AUTHOR_IN_REDUX, PUT_LIST_SEARCH, RATE_ITEM_IN_REDUX, PUT_QUERY_SEARCH,
    PUT_LIST_CABINET_IN_REDUX

} from '../../actions/content'

const initialState = {
    listFiltered: [],
    listTopAudio: [],
    listTopVideo: [],
    listTopText: [],
    listTopAuthor: [],
    listCabinet: [],
    listSearch: [],
    searchQuery: "",
    pages: 1,

    openedItem: {
        itemNo: "",
        type: "",
        subType: "",
        authorID: "",
        authorName: "",
        itemUrl: "",
        title: "",
        description: "",
        imageUrl: "",
        source: "",
        content: "",
        rate: "",
    }
}

function content(state = initialState, action) {

    switch (action.type) {

        case PUT_LIST_IN_REDUX:

            state.listFiltered = [];
            return {
                ...state,
                listFiltered: action.payload.data,
                pages: action.payload.amount
            }

        case PUT_LIST_AUDIO_IN_REDUX:

            state.listTopAudio = [];
            return {
                ...state,
                listTopAudio: action.payload.data,
            }

        case PUT_LIST_VIDEO_IN_REDUX:

            state.listTopVideo = [];
            return {
                ...state,
                listTopVideo: action.payload.data,
            }

        case PUT_LIST_TEXT_IN_REDUX:

            state.listTopText = [];
            return {
                ...state,
                listTopText: action.payload.data,
            }
        case PUT_LIST_AUTHOR_IN_REDUX:

            state.listTopAuthor = [];
            return {
                ...state,
                listTopAuthor: action.payload.data,
            }

        case PUT_LIST_CABINET_IN_REDUX:

            // console.log('action.payload.data', action.payload.data)

            state.listCabinet = [];
            return {
                ...state,
                listCabinet: action.payload.data,
            }
        case PUT_LIST_SEARCH:


            state.listSearch = [];
            return {
                ...state,
                listSearch: action.payload.data,
            }

        case PUT_QUERY_SEARCH:

            // console.log('action.payload.search', action.payload.search)

            return {
                ...state,
                searchQuery: action.payload.search,
            }


        case PUT_ITEM_IN_REDUX:

            // console.log('PUT_ITEM_IN_REDUX')
            return {
                ...state,
                openedItem: {...action.payload.data},
            }

        case RATE_ITEM_IN_REDUX:
            return {
                ...state,
                openedItem: {...state.openedItem,
                    rate: action.payload.data
                },
            }

        default:
            return {...state}
    }
}


export default content