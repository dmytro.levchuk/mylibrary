import React, {Component} from 'react'
import {connect} from 'react-redux'

import ReactHtmlParser, {processNodes, convertNodeToElement, htmlparser2} from 'react-html-parser';

import {
    setNewItemRate,
    getItemInfo,
    getContentAuthorList
} from "../../actions/content";

import Footer from "../Footer";


import ItemCard from "../ItemCard"

import './ItemPage.scss'
import {OPEN_SIGNIN_FORM} from "../../actions/modals";

class ItemPage extends Component {

    state = {
        // type: "",
        // subType: "",
        // itemNo: "",

        rate: true,
        rateBtn: true,
        rateChoose: false,
        rateCurrent: false,

        chosenRate: 0,
        author: false,

    }

    componentDidMount() {

        let {type, subType, itemNo} = this.props.match.params;

        this.props.getItemInfo({itemNo});

        // let author = this.props.openedItem.authorName;
        // console.log(author)
        // this.props.getContentAuthorList({author})


    };

    onMouseLeaveChoose = () => {
        this.setState({
            rate: true,
            rateBtn: true,
            rateChoose: false,
            rateCurrent: false,

            chosenRate: 0
        });
    }

    onMouseEnterChoose = (e) => {

        this.setState({
            rateBtn: false,
            rateCurrent: true,

            chosenRate: Number(e.target.dataset.btn),
        });

    }

    showRateChoose = () => {

        // console.log('click btn')

        this.setState({
            rate: false,
            rateChoose: true,
        });
    }

    sendNewRate = () => {
        let rate = this.state.chosenRate;

        // console.log(rate);

        // console.log(this.props.openedItem.authorID);

        if (this.props.loginInfo.isLogged) {
            this.props.setNewItemRate({
                rate: rate,
                authorID: this.props.loginInfo.loggedData.id,
                itemNo: this.props.openedItem.itemNo,
            })
        } else {
            this.props.openSignInForm();
        }

        this.setState({
            rate: true,
            rateBtn: true,
            rateChoose: false,
            rateCurrent: false,

            chosenRate: 0
        });


    }


    render() {

        // console.log("this.props.openedItem", this.props.openedItem)


        let rateDB = this.props.openedItem.rate;

        let chooseBtns = [];
        for (let i = 1; i <= 10; i++) {
            let key_i = "star" + i;

            let elem = (i <= this.state.chosenRate) ?
                (
                    <div
                        data-btn={i}
                        className="item-page__rating_littleStar item-page__rating_littleStar_fill"
                        key={key_i}>
                    </div>
                ) :

                (
                    <div
                        data-btn={i}
                        className="item-page__rating_littleStar item-page__rating_littleStar_empty"
                        key={key_i}>
                    </div>
                )

            chooseBtns.push(elem);


        }


        let classRate = (this.state.rate) ? "item-page__rating_rate" : "d-none",
            classRateBtn = (this.state.rateBtn) ? "item-page__rating_rateBtn" : "d-none",
            classRateChoose = (this.state.rateChoose) ? "item-page__rating_rateChoose" : "d-none",
            classRateCurrent = (this.state.rateCurrent) ? "item-page__rating_rateCurrent" : "d-none";

        let imgBlock;

        if (this.props.openedItem.authorName && !this.state.author) {

            let authorName = this.props.openedItem.authorName;
            // console.log(authorName)
            this.props.getContentAuthorList({authorName})

            this.setState({
                author: true,
            })
        }


        switch (this.props.openedItem.type) {

            case "text":
                imgBlock = (<img src={this.props.openedItem.imageUrl} alt=""/>)
                break;
            case "audio":

                let linkAudio = this.props.openedItem.source;

                let sound = "https://w.soundcloud.com/player/?url=https%3A//" + linkAudio
                    + "&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"

                imgBlock = (<iframe width="100%" height="100%" scrolling="no" frameBorder="no" allow="autoplay"
                                    src={sound}>

                </iframe>)

                break;

            case "video":

                let linkVideo = this.props.openedItem.source;

                let video = "https://www.youtube.com/embed/" + linkVideo;

                imgBlock = (
                    <iframe width="100%" height="350" src={video} frameBorder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen>

                    </iframe>
                )

                break;


        }

        let miniAuthorListItems = []

        if (this.props.listAuthor.length > 0) {
            miniAuthorListItems = this.props.listAuthor.map((elem) => {

                return (
                    <ItemCard
                        itemUrl={elem.itemUrl}
                        type={elem.type}
                        authorName={elem.authorName}
                        title={elem.title}
                        description={elem.description}
                        rate={elem.rate}
                        imageUrl={elem.imageUrl}
                        key={elem.itemUrl}
                    />
                )


            });
        }

        return (
            <div>
                <div className="container">
                    <div className="item-page">

                        <div className="item-page__block">
                            <h2 className="item-page__title">
                                {this.props.openedItem.title}
                            </h2>

                            <div className="item-page__social">
                                <div className="item-page__social_links">


                                    <img className="social__links_item" src="/img/facebook.png" alt=""/>
                                    <img className="social__links_item" src="/img/instagram.png" alt=""/>
                                    <img className="social__links_item" src="/img/twitter.png" alt=""/>
                                    <img className="social__links_item" src="/img/linkedin.png" alt=""/>
                                    <img className="social__links_item" src="/img/youtube_icon.png" alt=""/>

                                </div>

                                <div className="item-page__social_rate"
                                     onMouseLeave={() => this.onMouseLeaveChoose()}>

                                    <div className={classRate}>
                                        <div className="item-page__rating_bigStar item-page__rating_bigStar_fill">

                                        </div>
                                        <div className="item-page__rating_text">
                                            {rateDB}
                                        </div>
                                    </div>
                                    <div className={classRateBtn}
                                         onClick={() => this.showRateChoose()}
                                    >
                                        <div className="item-page__rating_bigStar item-page__rating_bigStar_empty">

                                        </div>

                                        <div className="item-page__rating_text">
                                            Rate
                                        </div>
                                    </div>
                                    <div className={classRateChoose}
                                         onClick={() => this.sendNewRate()}
                                         onMouseOver={(e) => this.onMouseEnterChoose(e)}
                                         onMouseLeave={() => this.onMouseLeaveChoose()}
                                    >
                                        {chooseBtns}
                                    </div>
                                    <div className={classRateCurrent}>
                                        <div className="item-page__rating_bigStar item-page__rating_bigStar_fill">

                                        </div>
                                        <div className="item-page__rating_text">
                                            {this.state.chosenRate}
                                        </div>
                                    </div>


                                </div>

                            </div>


                            <div className="item-page__img">

                                {imgBlock}
                            </div>

                            <div className="item-page__description">
                                {this.props.openedItem.description}
                            </div>

                            <div className="item-page__content">
                                {ReactHtmlParser(this.props.openedItem.content)}
                            </div>
                        </div>


                        <div className="item-page__list">
                            <div className="item-page__list-title">
                                Author's New Uploads
                            </div>

                            <div className="item-page__list-items">
                                {miniAuthorListItems}
                            </div>


                        </div>


                    </div>
                </div>

                <Footer/>
            </div>


        )
    }

}


const mapStateToProps = (state) => {
    return {
        openedItem: state.content.openedItem,
        listAuthor: state.content.listTopAuthor,
        loginInfo: state.users,
    }

}

const mapDispatchToProps = (dispatch) => {
    return {


        getContentAuthorList: (query) => dispatch(getContentAuthorList(query)),
        getItemInfo: (query) => dispatch(getItemInfo(query)),
        setNewItemRate: (query) => dispatch(setNewItemRate(query)),
        openSignInForm: () => {
            dispatch({type: OPEN_SIGNIN_FORM})
        },

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemPage)