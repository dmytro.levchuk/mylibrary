import React, {Component} from 'react'

import {NavLink} from "react-router-dom";

import "./BreadCrumbs.scss"

class BreadCrumbs extends Component {

    render () {

        // console.log(this.props.urls)

        let linkType = "/filter/" + this.props.urls.type + "/1";
        let linkSubType = "/filter/"+ this.props.urls.type + "/" + this.props.urls.subType + "/1";

        return (
            <div className="breadcrumbs">
                <NavLink to="/" className="breadcrumbs__item">
                    Home
                </NavLink>
                <NavLink to={linkType} className="breadcrumbs__item">
                    {this.props.urls.type}
                </NavLink>

                {this.props.urls.subType && (
                    <NavLink to={linkSubType} className="breadcrumbs__item">
                        {this.props.urls.subType}
                    </NavLink>
                )}


            </div>
        )
    }
}

export default BreadCrumbs