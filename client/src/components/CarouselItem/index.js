import React, {Component} from 'react'
import "react-responsive-carousel/lib/styles/carousel.min.css";

import {Carousel} from "react-responsive-carousel";

import "./CarouselItem.scss"

class CarouselItem extends Component {

    render() {

        let arrCarousel = [
            {
                img: "/img/carousel_audio.png",
                link: "",
                text: "You can add new audio content",
            },
            {
                img: "/img/carousel_video.png",
                link: "",
                text: "You can add new video content",
            },
            {
                img: "/img/carousel_text.png",
                link: "",
                text: "You can add new text content",
            },
        ];

        let carouselItems = arrCarousel.map((elem, index) => {

            let key_car = "carousel" + index;

            return (
                <div className="carousel-block__item"
                     key={key_car}>
                    <img src={elem.img} alt=""/>
                </div>
            )
        })

        return (
            <div>
                <Carousel
                    className="carousel__block"
                    autoPlay={true}
                    interval={5000}
                    transitionTime={700}
                    stopOnHover={true}
                    showIndicators={false}
                    infiniteLoop={true}
                    showStatus={false}
                    useKeyboardArrows={true}
                    showThumbs={false}
                >
                    {carouselItems}
                </Carousel>
            </div>
        )
    }

}


export default CarouselItem
