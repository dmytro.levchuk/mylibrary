import React, {Component} from 'react'

import './Contacts.scss'

class Contacts extends Component {


    render() {

        return (
            <div className="container">
                <div className="contacts">
                    <div className="contacts-title">
                        Contacts
                    </div>

                    <div className="contacts-content">
                        <div className="mapouter">
                            <div className="gmap_canvas">
                                <iframe width="600" height="500" id="gmap_canvas"
                                        src="https://maps.google.com/maps?q=kyiv%20%D1%88%D0%B5%D0%B2%D1%87%D0%B5%D0%BD%D0%BA%D0%B0%2022&t=&z=13&ie=UTF8&iwloc=&output=embed"
                                        frameBorder="0" scrolling="no" marginHeight="0" marginWidth="0">

                                </iframe>
                                <a href="https://www.embedgooglemap.org">embedgooglemap.org</a></div>

                        </div>

                        <div className="contacts-details">
                            <p>
                                Tarasa Shevchenko ave, 17A Kyiv, Ukraine
                            </p>
                            <p>
                                +38 067 552 56 43
                            </p>
                            <p>
                                content@google.com
                            </p>




                        </div>
                    </div>


                </div>
            </div>

        )
    }
}

export default Contacts