import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';

import {requiredInput} from '../../../validation'
import customRequiredInput from '../../atomic/customRequiredInput';

class FormReduxSingIn extends Component {
    render() {
        const {handleSubmit} = this.props;

        return (
            <form onSubmit={handleSubmit} className='sign-form'>

                <div className='field-wrapper'>

                    <Field name="email" component={customRequiredInput}
                           className='sign-form__input'
                           classError='sign-form__error-msg'
                           type="text" placeholder="Your email"
                           validate={[requiredInput]}
                    />
                </div>
                <div className='field-wrapper'>
                    <Field name="password" component={customRequiredInput}
                           className='sign-form__input'
                           classError='sign-form__error-msg'
                           type="password" placeholder="Your password"
                           validate={[requiredInput]}
                    />
                </div>

                <button name='signSbm' type="submit" className='sign-form__btn' label="submit">Sign In</button>
            </form>
        );


    }

}

export default reduxForm({form: 'formSignin'})(FormReduxSingIn);