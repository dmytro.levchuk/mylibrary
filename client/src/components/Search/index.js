import React, {Component} from 'react'

import {connect} from 'react-redux'

import ItemCard from "../ItemCard"

import {getSearchList} from "../../actions/content";

import "./Search.scss"

class Search extends Component {

    state = {
        query: '',
        wasUpdate: false,
    }

    componentDidMount() {

        // console.log('searchQuery', this.props.searchQuery);
        let searchQuery = this.props.searchQuery;

        // let {type, subType, itemNo} = this.props.match.params;

        this.props.getSearchList({searchQuery});

        this.setState({
            query: searchQuery,
        })

    };

    componentWillUpdate(nextProps, nextState) {
        // console.log('shouldComponentUpdate')
        // console.log('nextProps.searchQuery', nextProps.searchQuery)
        // console.log('nextState.query', nextState.query)


        if (nextProps.searchQuery !== nextState.query) {

            let searchQuery = nextProps.searchQuery;
            this.props.getSearchList({searchQuery});

            this.setState({
                query: searchQuery,
            })

            // return true
            // console.log('searchQuery', this.props.searchQuery)
            // this.setState({
            //     getQuery: true,
            // })
        } else if (nextProps.listSearch.length > 0 && !nextState.wasUpdate) {
            this.setState({
                wasUpdate: true,
            })
            // return true
        } else {
            // return false
        }
    }


    render() {


        // if (this.props.searchQuery !== this.state.getQuery) {
        //     console.log('searchQuery', this.props.searchQuery)
        //     this.setState({
        //         getQuery: true,
        //     })
        // }

        let listItems = [];

        // console.log('listSearch', this.props.listSearch)


        if (this.props.listSearch.length > 0) {

            // console.log('listSearch', this.props.listSearch)
            listItems = this.props.listSearch.map((elem) => {

                return (
                    <ItemCard
                        itemUrl={elem.itemUrl}
                        type={elem.type}
                        authorName={elem.authorName}
                        title={elem.title}
                        description={elem.description}
                        rate={elem.rate}
                        imageUrl={elem.imageUrl}
                        key={elem.itemUrl}
                    />
                )


            });
        }

        let emptyState = (
            <div>
                Loading...
            </div>
        )


        return (
            <div className="container">
                <div className="search-title">
                    Results for request : {this.props.searchQuery}
                </div>

                <div className="search">
                    {this.props.listSearch.length < 1
                        ? emptyState
                        : listItems}
                </div>
            </div>

        )
    }

}


const mapStateToProps = (state) => {
    return {
        listSearch: state.content.listSearch,
        searchQuery: state.content.searchQuery,

    }

}

const mapDispatchToProps = (dispatch) => {
    return {

        getSearchList: (query) => dispatch(getSearchList(query)),


    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Search)