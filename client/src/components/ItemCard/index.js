import React, {Component} from 'react';

import {NavLink} from "react-router-dom";

import "./ItemCard.scss"

class ItemCard extends Component {

    render() {

        let {
            itemUrl,
            type,
            authorName,
            description,
            rate,
            title,
            imageUrl
        } = this.props;




        return (

            <NavLink to={itemUrl} className="item-card__link" key={itemUrl}>
                <div className="item-card">
                    <div className="item-card__img">
                        <img src={imageUrl} alt=""/>
                    </div>
                    <div className="item-card__title">
                        {title}
                    </div>
                    <div className="item-card__description">
                        {description}
                    </div>

                </div>
            </NavLink>


        )

    }

}

export default ItemCard