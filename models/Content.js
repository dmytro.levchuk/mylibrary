const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ContentSchema = new Schema({
    itemNo: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    subType: {
        type: String,
        required: true
    },
    authorName: {
        type: String,
        required: false
    },
    itemUrl: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    imageUrl: {
        type: String,
        required: false
    },
    source: {
        type: String,
        required: false
    },
    content: {
        type: String,
        required: false
    },
    itemFeatures: {
        rate: [
            {
                authorId: {
                    type: String,
                    required: true
                },
                authorRate: {
                    type: Number,
                    required: true
                }
            }
        ],
        comments: [
            {
                authorName: {
                    type: String,
                    required: true
                },
                authorComment: {
                    type: String,
                    required: true
                }
            }
        ]
    },
    date: {
        type: Date,
        default: Date.now
    }


});

ContentSchema.index({ "$**": "text" });

module.exports = Content = mongoose.model("content", ContentSchema);