import React, { Fragment } from 'react';

const radio = (props) => {
    const {input, type, className, id} = props;
    return (
        <Fragment>
            <input {...input} type={type} className={className} id={id}/>
        </Fragment>
    )
}
export default radio;
//
// import React, { Fragment } from 'react';
//
// const radio = (props) => {
//     const {input, options} = props;
//     return (
//         <Fragment>
//
//             {options.map(o =>
//                 <Fragment key={o.value}>
//                     <input {...input} type="radio" className="d-none" id={o.value}/>
//                     <div className="field-select__item">
//                         <label htmlFor={o.value}>{o.title}</label>
//                     </div>
//                 </Fragment>)
//
//             }
//         </Fragment>
//     )
// }
// export default radio;