import React, {Component} from 'react';

import {connect} from 'react-redux';

import FormReduxSingIn from './FormReduxSingIn'

import {
    checkLogin
} from "../../../actions/users";



import "./SignUpForm.scss";

class SignUpForm extends Component {

    handleSubmit = (values) => {
        let sendLogin = {
            email: values.email,
            password: values.password
        };

        this.props.checkLogin(sendLogin);

    }

    render() {

        let classCheckEmail = (this.props.windowsStatus.invalidLogin) ? 'modal__error_msg' : 'd-none';

        return (
            <div className='modal__singin'>
                <div data-btn="btn-signin-close"
                     className="modal__close"
                     onClick={() => document.body.style.overflow = "auto"}>
                    <div data-btn="btn-signin-close" className="btn-close__line-top">
                    </div>
                    <div data-btn="btn-signin-close" className="btn-close__line-bottom">
                    </div>
                </div>
                <h2 className='modal__title'>Sign In</h2>
                <div className={classCheckEmail}>
                    Invalid Email or password
                </div>

                <FormReduxSingIn onSubmit={this.handleSubmit}/>

                <div className="modal__or">
                    or
                </div>

                <input name='linkRegistration' data-btn="btn-sign-link-reg"
                       type="button" className='sign-form__btn sign-form__btn_light' label="submit"
                       value="Create New Account"/>


            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        windowsStatus: state.modals.windowsStatus,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        checkLogin: (loginForm) => dispatch(checkLogin(loginForm)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUpForm)