const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const passport = require("passport");

const users = require("./routes/users");
const content = require("./routes/content");


const app = express();
const path = require("path");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, "client/build")));

const db = require("./config/keys").mongoURI;


mongoose
    .connect(db, { useNewUrlParser: true })
    .then(() => console.log("MongoDB Connected"))
    .catch(err => console.log(err));

// Passport middleware
app.use(passport.initialize());

// Passport Config
require("./config/passport")(passport);

//Connect routes
app.use("/", users);
app.use("/", content);

const port = process.env.PORT || 4000;

let server = app.listen(port, () =>
    console.log(`Server running on port ${port}`)
);

// console.log(process.env.NODE_ENV);
module.exports = server;