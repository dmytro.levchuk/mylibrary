import React, {Component} from "react";
import {connect} from "react-redux";

import ItemCard from "../ItemCard"
import BreadCrumbs from "../BreadCrumbs"

import {NavLink} from "react-router-dom";

import {
    getContentList
} from "../../actions/content";

import "./ListItems.scss"
import content from "../../reducers/content";

class ListItems extends Component {

    state = {
        type: "",
        subType: "",
        page: 1,
    }

    componentDidMount() {
        let {type, subType, page} = this.props.match.params;

        this.props.getContentList({type, subType, page});

        this.setState({
            type, subType, page
        });

    };

    // windowResize = () => {
    //     let a = document.getElementsByClassName("list-items__block");
    //
    //
    //     // document.getElementById("list-items__filter").
    //
    //     console.log(a[0].offsetLeft)
    //
    // }


    render() {

        // window.addEventListener("resize", this.windowResize);

        let {type, subType, page} = this.props.match.params;

        if (this.state.type !== this.props.match.params.type ||
            this.state.subType !== this.props.match.params.subType ||
            this.state.page !== this.props.match.params.page) {

            this.props.getContentList({type, subType, page});

            this.setState({
                type: this.props.match.params.type,
                subType: this.props.match.params.subType,
                page: this.props.match.params.page,
            });
        }

        let listItems = [];

        if (this.props.content.length > 0) {
            listItems = this.props.content.map((elem) => {

                return (
                    <ItemCard
                        itemUrl={elem.itemUrl}
                        type={elem.type}
                        authorName={elem.authorName}
                        title={elem.title}
                        description={elem.description}
                        rate={elem.rate}
                        imageUrl={elem.imageUrl}
                        key={elem.itemUrl}
                    />
                )


            });
        }
        let linkPages = [];

        if (this.props.amountPages > 1) {

            for (let i = 1; i <= this.props.amountPages; i++) {

                let link = "/filter/" + this.state.type + "/";

                if (this.state.subType) {
                    link += this.state.subType + "/";
                }

                link += i;


                let elem = (
                    <NavLink to={link} key={link}
                             className="list-items__pagination-link"
                             activeClassName="list-items__pagination-link-active"
                    >
                        {i}
                    </NavLink>
                )

                linkPages.push(elem);


            }

        }

        let emptyState = (
            <div>
                Loading...
            </div>
        )


        return (
            <div className="container">

                <BreadCrumbs urls={this.props.match.params}/>

                <div className="list-items__block">
                    <div className="list-items__filter" id="list-items__filter">
                        <NavLink to="/filter/audio/1" className="filter__menu-item"
                                 activeClassName="filter__menu_active">
                            Audio
                        </NavLink>
                        <NavLink to="/filter/audio/music/1" className="filter__submenu-item"
                                 activeClassName="filter__menu_active">
                            Music
                        </NavLink>
                        <NavLink to="/filter/audio/sport/1" className="filter__submenu-item"
                                 activeClassName="filter__menu_active">
                            Sport
                        </NavLink>
                        <NavLink to="/filter/audio/economic/1" className="filter__submenu-item"
                                 activeClassName="filter__menu_active">
                            Economic
                        </NavLink>
                        <NavLink to="/filter/audio/art/1" className="filter__submenu-item"
                                 activeClassName="filter__menu_active">
                            Art
                        </NavLink>
                        <NavLink to="/filter/audio/nature/1" className="filter__submenu-item"
                                 activeClassName="filter__menu_active">
                            Nature
                        </NavLink>
                        <NavLink to="/filter/video/1" className="filter__menu-item"
                                 activeClassName="filter__menu_active">
                            Video
                        </NavLink>
                        <NavLink to="/filter/video/music/1" className="filter__submenu-item"
                                 activeClassName="filter__menu_active">
                            Music
                        </NavLink>
                        <NavLink to="/filter/video/sport/1" className="filter__submenu-item"
                                 activeClassName="filter__menu_active">
                            Sport
                        </NavLink>
                        <NavLink to="/filter/video/economic/1" className="filter__submenu-item"
                                 activeClassName="filter__menu_active">
                            Economic
                        </NavLink>
                        <NavLink to="/filter/video/art/1" className="filter__submenu-item"
                                 activeClassName="filter__menu_active">
                            Art
                        </NavLink>
                        <NavLink to="/filter/video/nature/1" className="filter__submenu-item"
                                 activeClassName="filter__menu_active">
                            Nature
                        </NavLink>
                        <NavLink to="/filter/text/1" className="filter__menu-item"
                                 activeClassName="filter__menu_active">
                            Text
                        </NavLink>
                        <NavLink to="/filter/text/music/1" className="filter__submenu-item"
                                 activeClassName="filter__menu_active">
                            Music
                        </NavLink>
                        <NavLink to="/filter/text/sport/1" className="filter__submenu-item"
                                 activeClassName="filter__menu_active">
                            Sport
                        </NavLink>
                        <NavLink to="/filter/text/economic/1" className="filter__submenu-item"
                                 activeClassName="filter__menu_active">
                            Economic
                        </NavLink>
                        <NavLink to="/filter/text/art/1" className="filter__submenu-item"
                                 activeClassName="filter__menu_active">
                            Art
                        </NavLink>
                        <NavLink to="/filter/text/nature/1" className="filter__submenu-item"
                                 activeClassName="filter__menu_active">
                            Nature
                        </NavLink>


                    </div>


                    <div className="list-items__content">

                        {this.props.content.length < 1
                            ? emptyState
                            : listItems}

                    </div>
                </div>

                {this.props.amountPages > 1 && (
                    <div className="list-items__pagination">
                        {linkPages}
                    </div>
                )}




            </div>

        )
    }
}


const mapStateToProps = (state) => {

    return {
        loginInfo: state.users,
        content: state.content.listFiltered,
        amountPages: state.content.pages,
    }

};

const mapDispatchToProps = (dispatch) => {
    return {

        getContentList: (query) => dispatch(getContentList(query)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListItems)

