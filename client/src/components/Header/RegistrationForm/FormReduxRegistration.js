import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';

import {requiredInput, correctEmail, matchPasswordsReg} from '../../../validation'
import customRequiredInput from '../../atomic/customRequiredInput';

class FormReduxRegistration extends Component {


    render () {
        const {handleSubmit} = this.props;


        return (
            <form onSubmit={handleSubmit} className='sign-form'>

                <div className='field-wrapper'>

                    <Field name="name" component={customRequiredInput}
                           className='sign-form__input'
                           classError='sign-form__error-msg'
                           type="text" placeholder="Your name"
                           validate={[requiredInput]}
                    />
                </div>
                <div className='field-wrapper'>

                    <Field name="email" component={customRequiredInput}
                           className='sign-form__input'
                           classError='sign-form__error-msg'
                           type="text" placeholder="Your email"
                           validate={[requiredInput, correctEmail]}
                    />
                </div>
                <div className='field-wrapper'>
                    <Field name="password" component={customRequiredInput}
                           className='sign-form__input'
                           classError='sign-form__error-msg'
                           type="password" placeholder="Your password"
                           validate={[requiredInput]}
                    />
                </div>
                <div className='field-wrapper'>
                    <Field name="password2" component={customRequiredInput}
                           className='sign-form__input'
                           classError='sign-form__error-msg'
                           type="password" placeholder="Repeat password"
                           validate={[requiredInput, matchPasswordsReg]}
                    />
                </div>

                <button name='signSbm' type="submit" className='sign-form__btn' label="submit">
                    Create New Account
                </button>
            </form>
        )

    }


}


export default reduxForm({form: 'formRegistration'})(FormReduxRegistration);