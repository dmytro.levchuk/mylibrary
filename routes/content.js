const express = require("express");
const router = express.Router();
const Content = require("../models/Content");

const uniqueRandom = require("unique-random");
const rand = uniqueRandom(0, 99999999);

router.post("/content/add", (req, res) => {

    const newItem = {...req.body};
    newItem.itemNo = rand();

    newItem.itemUrl = `/content/${newItem.type}/${newItem.subType}/${newItem.itemNo}`;

    const dbContent = new Content(newItem);

    dbContent
        .save()
        .then(data => res.json(data))
        .catch(err => console.log(err));
});

router.post("/content/getList", (req, res) => {

    let query = {}

    if (req.body.type) query.type = req.body.type;
    if (req.body.subType) query.subType = req.body.subType;

    let pageNo = req.body.page;

    let perPage = 9;

    Content
        .find(query)
        .sort({date: -1})
        .skip(perPage * pageNo - perPage)
        .limit(perPage)
        .then(data => {
            Content
                .find(query)
                .count()
                .then(amount => {
                    // console.log(amount);

                    let resData = [];
                    data.forEach((elem) => {

                        let obj = {};
                        obj.itemUrl = elem.itemUrl;
                        obj.type = elem.type;

                        obj.authorName = elem.authorName;
                        obj.title = elem.title;
                        obj.description = elem.description;
                        obj.imageUrl = elem.imageUrl;

                        if (elem.type === 'audio') obj.imageUrl = '/img/soundcloud.png';
                        if (elem.type === 'video') obj.imageUrl = '/img/youtube.png';


                        let counterRate = 0;
                        let sumRate = 0;

                        elem.itemFeatures.rate.forEach((rate) => {
                            counterRate++;
                            sumRate += rate.authorRate;
                        });
                        obj.rate = (sumRate / counterRate).toFixed(1);

                        if (isNaN(obj.rate)) {
                            obj.rate = 0
                        }

                        resData.push(obj);

                    })

                    // console.log('amount', amount)

                    res.json({
                        resData,
                        amount: amount / perPage
                    })



                    // res.json({
                    //     products: products,
                    //     amount: amount / perPage
                    // });
                });



            }
        )
        .catch(err => console.log(err));
});


router.post("/content/getMiniList", (req, res) => {

    let query = {}

    if (req.body.type) query.type = req.body.type;

    Content
        .find(query)
        .sort({date: -1})
        .limit(4)
        .then(data => {

                // console.log(data)
                let resData = [];
                data.forEach((elem) => {

                    let obj = {};
                    obj.itemUrl = elem.itemUrl;
                    obj.type = elem.type;

                    obj.authorName = elem.authorName;
                    obj.title = elem.title;
                    obj.description = elem.description;
                    obj.imageUrl = elem.imageUrl;

                    if (elem.type === 'audio') obj.imageUrl = '/img/soundcloud.png';
                    if (elem.type === 'video') obj.imageUrl = '/img/youtube.png';


                    let counterRate = 0;
                    let sumRate = 0;

                    elem.itemFeatures.rate.forEach((rate) => {
                        counterRate++;
                        sumRate += rate.authorRate;
                    });
                    obj.rate = (sumRate / counterRate).toFixed(1);

                    if (isNaN(obj.rate)) {
                        obj.rate = 0
                    }

                    resData.push(obj);

                })


                res.json({resData})
            }
        )
        .catch(err => console.log(err));
});


router.post("/content/getAuthorMiniList", (req, res) => {

    let query = {}

    query.authorName = req.body.authorName;

    Content
        .find(query)
        .sort({date: -1})
        .limit(3)
        .then(data => {

                // console.log(data)
                let resData = [];
                data.forEach((elem) => {

                    let obj = {};
                    obj.itemUrl = elem.itemUrl;
                    obj.type = elem.type;

                    obj.authorName = elem.authorName;
                    obj.title = elem.title;
                    obj.description = elem.description;
                    obj.imageUrl = elem.imageUrl;

                    if (elem.type === 'audio') obj.imageUrl = '/img/soundcloud.png';
                    if (elem.type === 'video') obj.imageUrl = '/img/youtube.png';


                    let counterRate = 0;
                    let sumRate = 0;

                    elem.itemFeatures.rate.forEach((rate) => {
                        counterRate++;
                        sumRate += rate.authorRate;
                    });
                    obj.rate = (sumRate / counterRate).toFixed(1);

                    if (isNaN(obj.rate)) {
                        obj.rate = 0
                    }

                    resData.push(obj);

                })


                res.json({resData})
            }
        )
        .catch(err => console.log(err));
});

router.post("/content/getAuthorList", (req, res) => {

    let query = {}

    query.authorName = req.body.authorName;

    // console.log('req.body.authorName', req.body.authorName)

    Content
        .find(query)
        .sort({date: -1})
        .then(data => {

                // console.log(data)
                let resData = [];
                data.forEach((elem) => {

                    let obj = {};
                    obj.itemUrl = elem.itemUrl;
                    obj.type = elem.type;

                    obj.authorName = elem.authorName;
                    obj.title = elem.title;
                    obj.description = elem.description;
                    obj.imageUrl = elem.imageUrl;

                    if (elem.type === 'audio') obj.imageUrl = '/img/soundcloud.png';
                    if (elem.type === 'video') obj.imageUrl = '/img/youtube.png';


                    let counterRate = 0;
                    let sumRate = 0;

                    elem.itemFeatures.rate.forEach((rate) => {
                        counterRate++;
                        sumRate += rate.authorRate;
                    });
                    obj.rate = (sumRate / counterRate).toFixed(1);

                    if (isNaN(obj.rate)) {
                        obj.rate = 0
                    }

                    resData.push(obj);

                })


                res.json({resData})
            }
        )
        .catch(err => console.log(err));
});


router.post("/content/getSearchList", (req, res) => {

    let query = "\"" + req.body.searchQuery + "\"";

    let regex = new RegExp(req.body.searchQuery,'i');

    Content
        // .find({
        //     $text: { $search: query,
        //         $caseSensitive: false,}
        // })
        .find({
            $or: [
                {'type': regex},
                {'subType': regex},
                {'authorName': regex},
                {'title': regex},
                {'description': regex},
                {'content': regex},
            ]
        })
        .then(data => {

                let resData = [];
                data.forEach((elem) => {

                    let obj = {};
                    obj.itemUrl = elem.itemUrl;
                    obj.type = elem.type;

                    obj.authorName = elem.authorName;
                    obj.title = elem.title;
                    obj.description = elem.description;
                    obj.imageUrl = elem.imageUrl;

                    if (elem.type === 'audio') obj.imageUrl = '/img/soundcloud.png';
                    if (elem.type === 'video') obj.imageUrl = '/img/youtube.png';


                    let counterRate = 0;
                    let sumRate = 0;

                    elem.itemFeatures.rate.forEach((rate) => {
                        counterRate++;
                        sumRate += rate.authorRate;
                    });
                    obj.rate = (sumRate / counterRate).toFixed(1);

                    if (isNaN(obj.rate)) {
                        obj.rate = 0
                    }

                    resData.push(obj);

                })


                res.json({resData})
            }
        )
        .catch(err => console.log(err));
});

router.post("/content/getItemInfo", (req, res) => {

    let query = {}

    if (req.body.itemNo) query.itemNo = req.body.itemNo;

    Content
        .findOne(query)
        .then(elem => {


                let data = {};
                data.itemNo = elem.itemNo;
                data.type = elem.type;
                data.subType = elem.subType;
                data.authorID = elem.authorID;
                data.authorName = elem.authorName;
                data.itemUrl = elem.itemUrl;
                data.title = elem.title;
                data.description = elem.description;
                data.imageUrl = elem.imageUrl;
                data.source = elem.source;
                data.content = elem.content;

                if (elem.type === 'audio') data.imageUrl = '/img/soundcloud.png';
                if (elem.type === 'video') data.imageUrl = '/img/youtube.png';

                let counterRate = 0;
                let sumRate = 0;

                elem.itemFeatures.rate.forEach((rate) => {
                    counterRate++;
                    sumRate += rate.authorRate;
                });
                data.rate = (sumRate / counterRate).toFixed(1);

                if (isNaN(data.rate)) {
                    data.rate = 0
                }

                res.json({data})
            }
        )
        .catch(err => console.log(err));
});

router.post("/content/setNewItemRate", (req, res) => {

    let query = {
        itemNo: req.body.itemNo
    }

    // console.log("req.body",req.body)

    Content
        .findOne(query)
        .then(data => {

            // console.log(data);
            let flag = 0;

            data.itemFeatures.rate.forEach((elem) => {
                if (elem.authorId === req.body.authorID) {

                    elem.authorRate = req.body.rate
                    flag = 1;
                }

            })

            if (!flag) {
                console.log('hi')
                data.itemFeatures.rate.push({
                    authorRate: req.body.rate,
                    authorId: req.body.authorID
                })
            }

            let counterRate = 0;
            let sumRate = 0;

            data.itemFeatures.rate.forEach((rate) => {
                counterRate++;
                sumRate += rate.authorRate;
            });
            let newRate = (sumRate / counterRate).toFixed(1);

            if (isNaN(newRate)) {
                newRate = 0
            }

            // console.log("newRate", newRate)

            Content.update({_id: data._id}, {
                $set: {
                    itemFeatures: data.itemFeatures
                }
            }).then(() => {
                res.json({
                    newRate
                })
            })
                .catch(err => console.log(err));

            }
        )
        .catch(err => console.log(err));
});


module.exports = router;