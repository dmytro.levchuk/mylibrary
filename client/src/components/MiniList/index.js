import React, {Component} from 'react'
import {connect} from 'react-redux'

import {NavLink} from "react-router-dom";

import ItemCard from "../ItemCard"

import {
    getContentMiniList, PUT_LIST_AUDIO_IN_REDUX, PUT_LIST_TEXT_IN_REDUX, PUT_LIST_VIDEO_IN_REDUX
} from "../../actions/content";


class MiniList extends Component {

    componentDidMount = () => {
        let type = this.props.type;

        this.props.getContentMiniList({type});

        // this.setState({
        //     type, subType, page
        // });

    };

    render () {

        let miniListItems = [];

        let link = "/filter/" + this.props.type + "/1";

        // console.log(this.props.type);


        switch (this.props.type) {
            case 'audio':
                if (this.props.infoList.listTopAudio.length > 0) {
                    miniListItems = this.props.infoList.listTopAudio.map((elem) => {

                        return (
                            <ItemCard
                                itemUrl={elem.itemUrl}
                                type={elem.type}
                                authorName={elem.authorName}
                                title={elem.title}
                                description={elem.description}
                                rate={elem.rate}
                                imageUrl={elem.imageUrl}
                                key={elem.itemUrl}
                            />
                        )


                    });
                }
                break;
            case 'video':

                if (this.props.infoList.listTopVideo.length > 0) {
                    miniListItems = this.props.infoList.listTopVideo.map((elem) => {

                        return (
                            <ItemCard
                                itemUrl={elem.itemUrl}
                                type={elem.type}
                                authorName={elem.authorName}
                                title={elem.title}
                                description={elem.description}
                                rate={elem.rate}
                                imageUrl={elem.imageUrl}
                                key={elem.itemUrl}
                            />
                        )


                    });
                }
                break;
            case 'text':
                if (this.props.infoList.listTopText.length > 0) {
                    miniListItems = this.props.infoList.listTopText.map((elem) => {

                        return (
                            <ItemCard
                                itemUrl={elem.itemUrl}
                                type={elem.type}
                                authorName={elem.authorName}
                                title={elem.title}
                                description={elem.description}
                                rate={elem.rate}
                                imageUrl={elem.imageUrl}
                                key={elem.itemUrl}
                            />
                        )


                    });
                }
                break;

        }

        return (
            <div className="minilist">
                <h2 className="minilist__title">
                    {this.props.title}
                </h2>
                <div className="mainpage__minilist">

                    {miniListItems}
                </div>

                <NavLink to={link} className="btn-align">
                    <input type="button" className='sign-form__btn'
                           value="Show All"/>
                </NavLink>



            </div>

        )
    }
}

const mapStateToProps = (state) => {

    return {
        infoList: state.content,
    }
}

const mapDispatchToProps = (dispatch) => {

    return {

        getContentMiniList: (query) => dispatch(getContentMiniList(query)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MiniList)
