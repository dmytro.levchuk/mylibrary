import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';

import {requiredInput, requiredInputRadioType} from '../../../validation'
import customRequiredInput from '../../atomic/customRequiredInput';
import radio from '../../atomic/radio';
// import textarea from '../../atomic/textarea';
import { Editor } from '@tinymce/tinymce-react';
// const  { input} = React

class FormReduxAddNew extends Component {

    state = { content: '' };

    handleEditorChange = (content) => {

        console.log(content)
        this.setState({ content });
    }

    handleSubmitText = (values) => {

        values.preventDefault();
        // event.preventDefault();
        // console.log(values)


        let text = this.state.content;

        let obj = {
            ...values,
            text
        }

        console.log(obj)

        this.props.handleSubmit({
            ...values,
            text
        });

    }

    render() {

        const {handleSubmit, pristine, submitting} = this.props;

        return (
            <form onSubmit={this.handleSubmitText} className='add-new-form'>
                <div className='field-select field-select_row'>
                    <label className='field-select__name'>
                        Type
                    </label>
                    <div className="field-select__items">


                        {/*<Field component={radio} name="type" options={[*/}
                            {/*{ title: 'Audio', value: 'audio' },*/}
                            {/*{ title: 'Video', value: 'video' },*/}
                            {/*{ title: 'Text', value: 'text' },*/}
                        {/*]}*/}
                               {/*validate={[requiredInputRadioType]}*/}
                        {/*/>*/}


                        <Field name="type" component={radio} type="radio" value="audio" className="d-none"
                               id="audio"
                               validate={[requiredInputRadioType]}/>
                        <div className="field-select__item">
                            <label htmlFor="audio">Audio</label>
                        </div>
                        <Field name="type" component={radio} type="radio" value="video" className="d-none"
                               id="video"
                               validate={[requiredInputRadioType]}/>
                        <div className="field-select__item">
                            <label htmlFor="video">Video</label>
                        </div>
                        <Field name="type" component={radio} type="radio" value="text" className="d-none"
                               id="text"
                               validate={[requiredInputRadioType]}/>
                        <div className="field-select__item">
                            <label htmlFor="text">Text</label>
                        </div>

                    </div>

                </div>


                <div className='field-wrapper'>

                    <Field name="title" component={customRequiredInput}
                           className='sign-form__input'
                           classError='sign-form__error-msg'
                           type="text" placeholder="Title"
                           validate={[requiredInput]}
                    />
                </div>
                <div className='field-wrapper'>
                    <Field name="description" component={customRequiredInput}
                           className='sign-form__input'
                           classError='sign-form__error-msg'
                           type="password" placeholder="Description"
                           validate={[requiredInput]}
                    />
                </div>

                <div className='field-wrapper'>
                    <Field name="image" component={customRequiredInput}
                           className='sign-form__input'
                           classError='sign-form__error-msg'
                           type="password" placeholder="Image (URL)"
                           validate={[requiredInput]}
                    />
                </div>

                <div className='field-text'>
                    {/*<Field name="text" component={textarea}*/}
                           {/*// className='sign-form__input'*/}
                           {/*// classError='sign-form__error-msg'*/}

                    {/*/>*/}

                    <Editor
                        // apiKey="API_KEY"
                        // init={{ plugins: 'link table' }}
                        value={this.state.content}
                        onEditorChange={this.handleEditorChange}
                    />
                </div>

                <button  name='signSbm' type="submit" className='sign-form__btn' label="submit"
                        disabled={pristine || submitting}>Save</button>
            </form>
        );


    }
}

export default reduxForm({form: 'formAddNew'})(FormReduxAddNew);