import React, { Component } from "react";

import { NavLink } from "react-router-dom";

import CarouselItem from "../CarouselItem"
import MiniList from "../MiniList"
import Footer from "../Footer";

import "./mainpage.scss";


class MainPage extends Component {

    render() {

        return (
            <div>
                <div className="container">
                    <CarouselItem/>
                    <div className="mainpage">

                        <h2 className="mainpage__title">
                            New uploads
                        </h2>


                        <MiniList
                            type="audio"
                            title="Audio"
                        />

                        <MiniList
                            type="video"
                            title="Video"
                        />

                        <MiniList
                            type="text"
                            title="Text"
                        />

                    </div>
                </div>

                <Footer/>
            </div>


        )
    }


}

export default MainPage;