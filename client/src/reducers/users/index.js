import { SET_LOGGED, LOGOUT

} from '../../actions/users'



const initialState = {
    isLogged: false,
    loggedData: {
        id: '',
        name: '',
        email: '',
    },
}

function users(state = initialState, action) {
    switch (action.type) {
        case SET_LOGGED:

            return {
                ...state,
                isLogged: true,
                loggedData: {
                    id: action.payload.data._id,
                    name: action.payload.data.name,
                    email: action.payload.data.email,
                },

            }
        case LOGOUT: {
            return {
                isLogged: false,
                loggedData: {
                    id: '',
                    name: '',
                    email: '',
                },
            }
        }

        default:
            return {...state}

    }
}

export default users