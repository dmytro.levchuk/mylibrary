import React, { Component } from "react";

import { NavLink } from "react-router-dom";

import "./footer.scss";


class Footer extends Component {

    render() {

        return (
            <div className="footer">
                <div className="container">

                    <div className="footer__block">
                        <div className="footer__contacts">
                            <div className="footer__details">
                                <h2 className="footer__title">
                                    Contacts
                                </h2>
                                <p>
                                    Tarasa Shevchenko ave, 17A Kyiv, Ukraine
                                </p>
                                <p>
                                    +38 067 552 56 43
                                </p>
                                <p>
                                    content@google.com
                                </p>
                            </div>
                        </div>

                        <div className="footer__links">

                            <h2 className="footer__title">
                                Content
                            </h2>

                            <NavLink to="/filter/audio/1" className="footer__link">
                                Audio
                            </NavLink>
                            <NavLink to="/filter/video/1" className="footer__link">
                                Video
                            </NavLink>
                            <NavLink to="/filter/text/1" className="footer__link">
                                Text
                            </NavLink>
                        </div>


                    </div>


                </div>
            </div>
        )
    }


}

export default Footer;