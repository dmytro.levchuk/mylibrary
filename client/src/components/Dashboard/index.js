import React, {Component} from 'react';
import {connect} from "react-redux";
import {Switch, Route} from "react-router-dom";

import Header from "../Header";
import MainPage from "../MainPage";
import Footer from "../Footer";
import Cabinet from "../Cabinet"
import ListItems from "../ListItems"
import ItemPage from "../ItemPage"
import Search from "../Search"
import Contacts from "../Contacts"

class Dashboard extends Component {


    render() {
        return (
            <div>
                <Header/>
                <div className="dashboard">
                    <Switch>
                        <Route exact path="/content/:type/:subType/:itemNo" component={ItemPage}/>
                        <Route exact path="/filter/:type/:subType?/:page(\d+)" component={ListItems}/>
                        <Route path="/search" component={Search}/>
                        <Route path="/contacts" component={Contacts}/>
                        <Route path="/cabinet" component={Cabinet}/>
                        <Route path="/" component={MainPage}/>

                    </Switch>
                </div>


            </div>
        )
    }

}

export default Dashboard