const express = require("express");
const router = express.Router();
const Users = require("../models/Users");

const passport = require("passport");
const jwt = require("jsonwebtoken");
const keys = require("../config/keys");




router.post("/register", (req, res) => {

    // Check if the user already exist
    Users.findOne({email: req.body.email}).then(user => {
        if (user) {
            // if user allready exist then deny request from front
            res.json({result: false});
        } else {
            // If not exist - create user object according to mongoose schema

            const newUser = new Users({
                name: req.body.name,
                email: req.body.email,
                password: req.body.password
            });
            newUser
                .save()
                .then(user => {
                    res.json(user);
                })
                .catch(err => console.log(err));

        }
    });
});


//Send post-request from Login form
router.post("/login", (req, res) => {
    passport.authenticate("local", { session: false }, (err, user, info) => {
        if (err || !user) {
            return res.json({ success: false });
        }

        req.login(user, { session: false }, err => {
            if (err) {
                res.json({ success: false });
            }

            let payload = { ...user };

            jwt.sign(
                payload,
                keys.secretOrKey,
                { expiresIn: 172800 },
                (err, token) => {
                    res.json({
                        success: true,
                        token: "Bearer " + token
                    });
                }
            );
        });
    })(req, res);
});

module.exports = router;