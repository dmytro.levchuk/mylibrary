import React, {Component} from 'react';

class RegistrationOk extends Component {


    render () {

        return (
            <div className='modal__singin'>
                <div data-btn="btn-reg-ok-close"
                     className="modal__close"
                     onClick={() => document.body.style.overflow = "auto"}>
                    <div data-btn="btn-reg-ok-close" className="btn-close__line-top">
                    </div>
                    <div data-btn="btn-reg-ok-close" className="btn-close__line-bottom">
                    </div>
                </div>
                <h2 className='modal__title'>Your account has been created</h2>

                <input name='linkRegistration' data-btn="btn-reg-ok-close"
                       type="button" className='sign-form__btn' label="submit"
                       value="Close"/>

            </div>
        )
    }


}

export default RegistrationOk