import React, {Component} from "react";
import {connect} from "react-redux";

import {NavLink} from "react-router-dom";

import SignUpForm from "./SignUpForm"
import RegistrationForm from "./RegistrationForm"
import RegistrationOk from "./RegistrationOk"

import {
    PUT_QUERY_SEARCH
} from "../../actions/content";


import {
    logoutUser
} from "../../actions/users";

import {
    OPEN_SIGNIN_FORM,
    OPEN_REGISTRATION_FORM,
    CLOSE_REGISTRATION_FORM,
    CLOSE_SIGNIN_FORM,
    OPEN_PROFILE_WINDOW,
    CLOSE_PROFILE_WINDOW
} from "../../actions/modals";

import "./header.scss";


class Header extends Component {

    state = {
        search: '',
    };

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    sendSearch = () => {

        this.props.setSearchQuery(this.state.search);


    }

    clickOnMenu = (e) => {

        if (e.target.dataset.btn !== 'btn_submenu') {
            document.getElementById("show-navmenu").checked = false;
            document.getElementById("show-audio").checked = false;
            document.getElementById("show-video").checked = false;
            document.getElementById("show-text").checked = false;
        }

    }

    clickOnSignIn = () => {
        document.body.style.overflow = "hidden";
        this.props.openSignInForm();
    }

    handleSignInForm = (e) => {

        if (e.target.dataset.btn !== 'btn-signin-close') {
            e.stopPropagation();
        }

        if (e.target.dataset.btn === 'btn-sign-link-reg') {
            this.props.closeSignInForm();
            this.props.openRegistrationForm();
        }
    }

    handleRegistrationForm = (e) => {
        if (e.target.dataset.btn !== 'btn-registration-close') {
            e.stopPropagation();
        }
    }

    handleRegistrationOk = (e) => {
        if (e.target.dataset.btn !== 'btn-reg-ok-close') {
            e.stopPropagation();
        }
    }

    render() {

        return (
            <header className="header">
                <div className="container">
                    <div className="header__block">
                        <div className="header__logo-menu">
                            <input className="d-none" type="checkbox" id="show-navmenu"/>
                            {/*<input className="d-none" type="checkbox" id="show-login"/>*/}
                            <NavLink to="/" className="header__logo">
                                Logo
                            </NavLink>

                            <div className="header__center">

                                <label className="header__burger" htmlFor="show-navmenu">
                                    <div className="burger__line-top">
                                    </div>
                                    <div className="burger__line-centre">
                                    </div>
                                    <div className="burger__line-bottom">
                                    </div>
                                </label>

                                <div className="header__search">
                                    <input type="text" className="search__input"
                                           name="search"
                                           placeholder="Find Audio, Video, Text..."
                                           value={this.state.title}
                                           onChange={this.handleInputChange}/>
                                    <NavLink to="/search" className="search__icon"
                                        onClick={this.sendSearch}>
                                        <img src="/img/search.png" alt=""/>
                                    </NavLink>
                                </div>

                                <input className="d-none" type="checkbox" id="show-audio"/>
                                <input className="d-none" type="checkbox" id="show-video"/>
                                <input className="d-none" type="checkbox" id="show-text"/>

                                <ul className="navmenu"  onClick={(e) => this.clickOnMenu(e)}>
                                    <li className="navmenu__item">
                                        <NavLink to="/" className="navmenu__link">
                                            Home
                                        </NavLink>
                                    </li>
                                    <li className="navmenu__item">
                                        <div className="navmenu__item_full">
                                            <NavLink to="/filter/audio/1" className="navmenu__link">
                                                Audio
                                            </NavLink>
                                            <label data-btn="btn_submenu" className="submenu_arrow" htmlFor="show-audio">

                                            </label>
                                        </div>

                                        <div className="navmenu__submenu submenu__audio">
                                            <NavLink to="/filter/audio/music/1" className="navmenu__submenu_link">
                                                Music
                                            </NavLink>
                                            <NavLink to="/filter/audio/sport/1" className="navmenu__submenu_link">
                                                Sport
                                            </NavLink>
                                            <NavLink to="/filter/audio/economic/1" className="navmenu__submenu_link">
                                                Economic
                                            </NavLink>
                                            <NavLink to="/filter/audio/art/1" className="navmenu__submenu_link">
                                                Art
                                            </NavLink>
                                            <NavLink to="/filter/audio/nature/1" className="navmenu__submenu_link">
                                                Nature
                                            </NavLink>
                                        </div>
                                    </li>
                                    <li className="navmenu__item">
                                        <div className="navmenu__item_full">
                                            <NavLink to="/filter/video/1" className="navmenu__link">
                                                Video
                                            </NavLink>
                                            <label data-btn="btn_submenu" className="submenu_arrow" htmlFor="show-video">

                                            </label>
                                        </div>


                                        <div className="navmenu__submenu submenu__video">
                                            <NavLink to="/filter/video/music/1" className="navmenu__submenu_link">
                                                Music
                                            </NavLink>
                                            <NavLink to="/filter/video/sport/1" className="navmenu__submenu_link">
                                                Sport
                                            </NavLink>
                                            <NavLink to="/filter/video/economic/1" className="navmenu__submenu_link">
                                                Economic
                                            </NavLink>
                                            <NavLink to="/filter/video/art/1" className="navmenu__submenu_link">
                                                Art
                                            </NavLink>
                                            <NavLink to="/filter/video/nature/1" className="navmenu__submenu_link">
                                                Nature
                                            </NavLink>
                                        </div>
                                    </li>
                                    <li className="navmenu__item">
                                        <div className="navmenu__item_full">
                                            <NavLink to="/filter/text/1" className="navmenu__link">
                                                Text
                                            </NavLink>
                                            <label data-btn="btn_submenu" className="submenu_arrow" htmlFor="show-text">

                                            </label>
                                        </div>


                                        <div className="navmenu__submenu submenu__text">
                                            <NavLink to="/filter/text/music/1" className="navmenu__submenu_link">
                                                Music
                                            </NavLink>
                                            <NavLink to="/filter/text/sport/1" className="navmenu__submenu_link">
                                                Sport
                                            </NavLink>
                                            <NavLink to="/filter/text/economic/1" className="navmenu__submenu_link">
                                                Economic
                                            </NavLink>
                                            <NavLink to="/filter/text/art/1" className="navmenu__submenu_link">
                                                Art
                                            </NavLink>
                                            <NavLink to="/filter/text/nature/1" className="navmenu__submenu_link">
                                                Nature
                                            </NavLink>
                                        </div>
                                    </li>
                                    <li className="navmenu__item">
                                        <NavLink to="/contacts" className="navmenu__link">
                                            Contacts
                                        </NavLink>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div className="header__social">
                            <div className="social__links">

                                <img className="social__links_item" src="/img/facebook.png" alt=""/>
                                <img className="social__links_item" src="/img/instagram.png" alt=""/>
                                <img className="social__links_item" src="/img/twitter.png" alt=""/>
                                <img className="social__links_item" src="/img/linkedin.png" alt=""/>
                                <img className="social__links_item" src="/img/youtube_icon.png" alt=""/>
                            </div>

                            <div className="header__login">

                                {!this.props.loginInfo.isLogged && (
                                    <input type="button" value="Sign In"
                                           className="header__sign_btn"
                                           onClick={() => this.clickOnSignIn()}/>

                                )}

                                {this.props.loginInfo.isLogged && (
                                    <div className="header__logged"
                                         onClick={() => this.props.openProfileWindow()}
                                    >
                                        <img className="header__logged-img" src="/img/user_icon.png" alt=""/>
                                        <span className="header__logged-name">
                                            {this.props.loginInfo.loggedData.name}
                                            </span>

                                        {this.props.windowsStatus.profileWindow && (
                                            <ul className="header__logged-list"
                                                // onClick={() => this.props.closeProfileWindow()}
                                            >
                                                <li className="header__logged-item">
                                                    <NavLink to="/cabinet/content" className="header__logged-link">
                                                        My Content
                                                    </NavLink>
                                                </li>
                                                {/*<li className="header__logged-item">*/}
                                                    {/*<NavLink to="/cabinet/profile" className="header__logged-link">*/}
                                                        {/*Profile*/}
                                                    {/*</NavLink>*/}
                                                {/*</li>*/}
                                                <li className="header__logged-item">
                                                    <NavLink to="/" className="header__logged-link"
                                                             onClick={this.props.logoutUser}>
                                                        Sign Out
                                                    </NavLink>
                                                </li>
                                            </ul>

                                        )}

                                    </div>

                                )}

                            </div>

                        </div>

                    </div>


                    <div id="header-modal-form">
                        {this.props.windowsStatus.formSignInOpen && (
                            <div className="modal__background">
                                <div onClick={this.handleSignInForm}>
                                    <SignUpForm/>
                                </div>
                            </div>

                        )}

                        {this.props.windowsStatus.formRegistrationOpen && (
                            <div className="modal__background">
                                <div onClick={this.handleRegistrationForm}>
                                    <RegistrationForm/>
                                </div>
                            </div>

                        )}

                        {this.props.windowsStatus.registrationOk && (
                            <div className="modal__background">
                                <div onClick={this.handleRegistrationOk}>
                                    <RegistrationOk/>
                                </div>
                            </div>

                        )}

                    </div>


                </div>

            </header>
        )
    }


}


const mapStateToProps = (state) => {
    return {
        windowsStatus: state.modals.windowsStatus,
        loginInfo: state.users,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {

        openSignInForm: () => {
            dispatch({type: OPEN_SIGNIN_FORM})
        },
        openRegistrationForm: () => {
            dispatch({type: OPEN_REGISTRATION_FORM})
        },
        closeSignInForm: () => {
            dispatch({type: CLOSE_SIGNIN_FORM});
        },
        closeRegistrationForm: () => {
            dispatch({type: CLOSE_REGISTRATION_FORM});
        },

        openProfileWindow: () => {
            dispatch({type: OPEN_PROFILE_WINDOW})
        },

        closeProfileWindow: () => {
            dispatch({type: CLOSE_PROFILE_WINDOW})
        },

        logoutUser: () => dispatch(logoutUser()),

        setSearchQuery: (input) => {
            dispatch({type: PUT_QUERY_SEARCH, payload: {search: input}})
        },
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Header);
