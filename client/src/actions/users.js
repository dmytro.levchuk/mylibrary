import axios from 'axios';

import setAuthToken from "../utils/setAuthToken";
import jwt_decode from "jwt-decode";

// import store from "../store";

import {
    EXIST_USER, CLOSE_REGISTRATION_FORM, OPEN_REGISTRATION_OK, INCORRECT_LOGIN, CLOSE_SIGNIN_FORM
} from "./modals";

// import store from "../store";
export const SET_LOGGED = 'SET_LOGGED';

export const LOGOUT = 'LOGOUT';


export function checkUserOnStart() {

    return dispatch => {

        if (localStorage.jwtToken) {
            //Set the auth token header auth
            setAuthToken(localStorage.jwtToken);
            //Decode token and get user info and exp
            const decoded = jwt_decode(localStorage.jwtToken);
            const currentTime = Date.now() / 1000;
            // Check if token is out of date
            if (decoded.exp >= currentTime) {
                dispatch({
                    type: SET_LOGGED,
                    payload: {data: decoded._doc}
                });
            }
        }
    }
}


export function addNewUser(regForm) {
    return dispatch => {

        axios.post('/register', regForm)
            .then(res => res.data)
            .then(data => {

                    if (data.result === false) {
                        // if email is already used then show in form Message
                        dispatch({type: EXIST_USER})
                    } else {
                        // if registration is successfull - close window and show message
                        dispatch({type: CLOSE_REGISTRATION_FORM})
                        dispatch({type: OPEN_REGISTRATION_OK})
                    }
                }
            )
            .catch(err => console.log(err))
    }
}

export function checkLogin(loginForm) {
    return dispatch => {
        axios.post('/login', loginForm)
            .then(res => res.data)
            .then(data => {
                    if (data.success === false) {
                        dispatch({type: INCORRECT_LOGIN})
                    } else {
                        const {token} = data;
                        localStorage.setItem("jwtToken", token);
                        //Set token to Auth header
                        setAuthToken(token);
                        //Decode token to get user data
                        const decoded = jwt_decode(token);

                        dispatch({
                            type: SET_LOGGED,
                            payload: {data: decoded._doc}
                        });
                        dispatch({type: CLOSE_SIGNIN_FORM});
                        document.body.style.overflow = "auto";
                    }
                }
            )
            .catch(err => console.log(err))
    }
}

export const logoutUser = () => dispatch => {

    // Remove the token from local storage
    localStorage.removeItem("jwtToken");
    //Remove auth header for future requests
    setAuthToken(false);
    //Set the current user to an empty object
    dispatch({
        type: LOGOUT,
    });


};