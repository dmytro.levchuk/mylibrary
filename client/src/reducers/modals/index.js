import { SET_DEFAULT_WINDOWS, OPEN_REGISTRATION_OK, CLOSE_REGISTRATION_OK, INCORRECT_LOGIN,
    OPEN_SIGNIN_FORM, CLOSE_SIGNIN_FORM, OPEN_REGISTRATION_FORM, CLOSE_REGISTRATION_FORM, EXIST_USER,
    OPEN_PROFILE_WINDOW, CLOSE_PROFILE_WINDOW,
    OPEN_ADD_NEW_WINDOW, CLOSE_ADD_NEW_WINDOW

} from '../../actions/modals'

const initialState = {
    windowsStatus: {
        formSignInOpen: false,
        formRegistrationOpen: false,
        registrationOk: false,

        existEmail: false,
        invalidLogin: false,

        profileWindow: false,

        formAddNewOpen: false,


    }
}

function modals(state = initialState, action) {

    switch (action.type) {
        case OPEN_SIGNIN_FORM:
            return {
                ...state,
                windowsStatus: {
                    ...state.windowsStatus,
                    formSignInOpen: true,
                }
            }

        case CLOSE_SIGNIN_FORM:
            return {
                ...state,
                windowsStatus: {
                    ...state.windowsStatus,
                    formSignInOpen: false,
                }
            }

        case OPEN_REGISTRATION_FORM:
            return {
                ...state,
                windowsStatus: {
                    ...state.windowsStatus,
                    formRegistrationOpen: true,
                }
            }

        case CLOSE_REGISTRATION_FORM:
            return {
                ...state,
                windowsStatus: {
                    ...state.windowsStatus,
                    formRegistrationOpen: false,
                }
            }

        case EXIST_USER:
            return {
                ...state,
                windowsStatus: {
                    ...state.windowsStatus,
                    existEmail: true,
                }
            }

        case OPEN_REGISTRATION_OK:
            return {
                ...state,
                windowsStatus: {
                    ...state.windowsStatus,
                    registrationOk: true,
                }
            }

        case CLOSE_REGISTRATION_OK:
            return {
                ...state,
                windowsStatus: {
                    ...state.windowsStatus,
                    registrationOk: false,
                }
            }

        case INCORRECT_LOGIN:
            return {
                ...state,
                windowsStatus: {
                    ...state.windowsStatus,
                    invalidLogin: true,
                }
            }

        case OPEN_PROFILE_WINDOW:
            return {
                ...state,
                windowsStatus: {
                    ...state.windowsStatus,
                    profileWindow: true,
                }
            }

        case CLOSE_PROFILE_WINDOW:
            return {
                ...state,
                windowsStatus: {
                    ...state.windowsStatus,
                    profileWindow: false,
                }
            }

        case OPEN_ADD_NEW_WINDOW:
            return {
                ...state,
                windowsStatus: {
                    ...state.windowsStatus,
                    formAddNewOpen: true,
                }
            }

        case CLOSE_ADD_NEW_WINDOW:
            return {
                ...state,
                windowsStatus: {
                    ...state.windowsStatus,
                    formAddNewOpen: false,
                }
            }

        case SET_DEFAULT_WINDOWS:
            return {
                ...state,
                windowsStatus: {
                    formSignInOpen: false,
                    formRegistrationOpen: false,
                    registrationOk: false,

                    profileWindow: false,

                    existEmail: false,
                    invalidLogin: false,
                }
            }


        default:
            return {...state}

    }

}


export default modals