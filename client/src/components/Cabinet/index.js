import React, {Component} from 'react';
import {connect} from 'react-redux';

import {NavLink} from 'react-router-dom';
//
import {Switch, Route} from "react-router-dom";
// import {Switch, Route} from "react-router";
import Profile from "./Profile";
import Content from "./Content";

import {
    logoutUser
} from "../../actions/users";

import "./Cabinet.scss"

class Cabinet extends Component {

    render () {

        return (
            <div className="cabinet">
                <div className="container">
                    <div className="cabinet__block">
                        <div className="cabinet__menu">
                            <NavLink to="/cabinet/content"
                                     className="cabinet-menu__link">
                                My Content
                            </NavLink>
                            <NavLink to="/"
                                     onClick={this.props.logoutUser}
                                     className="cabinet-menu__link">
                                Logout
                            </NavLink>
                        </div>
                        <div className="cabinet__content">
                            <Route
                                exact
                                path="/cabinet/profile"
                                component={Profile}
                            />
                            <Route
                                exact
                                path="/cabinet/content"
                                component={Content}
                            />
                        </div>
                    </div>


                </div>


            </div>
        )
    }
}

const mapStateToProps = state => {
    return {

        // loginInfo: state.users,
    };
};

const mapDispatchToProps = dispatch => {
    return {

        logoutUser: () => dispatch(logoutUser()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cabinet)