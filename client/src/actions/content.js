import axios from 'axios';
import {CLOSE_ADD_NEW_WINDOW} from "./modals";

export const PUT_LIST_IN_REDUX = 'PUT_LIST_IN_REDUX';
export const PUT_LIST_AUDIO_IN_REDUX = 'PUT_LIST_AUDIO_IN_REDUX';
export const PUT_LIST_VIDEO_IN_REDUX = 'PUT_LIST_VIDEO_IN_REDUX';
export const PUT_LIST_TEXT_IN_REDUX = 'PUT_LIST_TEXT_IN_REDUX';
export const PUT_LIST_AUTHOR_IN_REDUX = 'PUT_LIST_AUTHOR_IN_REDUX';

export const PUT_LIST_CABINET_IN_REDUX = 'PUT_LIST_CABINET_IN_REDUX';

export const PUT_QUERY_SEARCH = 'PUT_QUERY_SEARCH';
export const PUT_LIST_SEARCH = 'PUT_LIST_SEARCH';

export const PUT_ITEM_IN_REDUX = 'PUT_ITEM_IN_REDUX';
export const RATE_ITEM_IN_REDUX = 'RATE_ITEM_IN_REDUX';

export function addNewContent(infoContent) {
    return dispatch => {

        // console.log(infoContent)

        axios.post('/content/add', infoContent)
            .then(data => {
                    dispatch({
                        type: CLOSE_ADD_NEW_WINDOW
                    });

                }
            )
            .catch(err => console.log(err))
    }
}


export function getContentList(query) {

    return dispatch => {

        axios.post('/content/getList', query)
            .then(res => res.data)
            .then(data => {

                    // console.log('data.amount', Math.ceil(data.amount))

                    dispatch({
                        type: PUT_LIST_IN_REDUX,
                        payload: {
                            data: data.resData,
                            amount: Math.ceil(data.amount)
                        }
                    });


                }
            )
            .catch(err => console.log(err))
    }

}

export function getSearchList(query) {

    return dispatch => {

        axios.post('/content/getSearchList', query)
            .then(res => res.data)
            .then(data => {

                    // console.log(data)
                    dispatch({
                        type: PUT_LIST_SEARCH,
                        payload: {data: data.resData}
                    });

                }
            )
            .catch(err => console.log(err))
    }

}

export function getContentMiniList(query) {

    return dispatch => {

        axios.post('/content/getMiniList', query)
            .then(res => res.data)
            .then(data => {

                    // console.log(data)

                    switch (query.type) {
                        case 'audio':
                            dispatch({
                                type: PUT_LIST_AUDIO_IN_REDUX,
                                payload: {data: data.resData}
                            });
                            break;
                        case 'video':
                            dispatch({
                                type: PUT_LIST_VIDEO_IN_REDUX,
                                payload: {data: data.resData}
                            });
                            break;
                        case 'text':
                            dispatch({
                                type: PUT_LIST_TEXT_IN_REDUX,
                                payload: {data: data.resData}
                            });
                            break;

                    }

                }
            )
            .catch(err => console.log(err))
    }

}

export function getCabinetAuthorList(query) {

    return dispatch => {

        axios.post('/content/getAuthorList', query)
            .then(res => res.data)
            .then(data => {
                    // console.log("data", data)

                    dispatch({
                        type: PUT_LIST_CABINET_IN_REDUX,
                        payload: {data: data.resData}
                    });
                    // console.log(data)

                }
            )
            .catch(err => console.log(err))
    }

}

export function getContentAuthorList(query) {

    return dispatch => {

        axios.post('/content/getAuthorMiniList', query)
            .then(res => res.data)
            .then(data => {

                    dispatch({
                        type: PUT_LIST_AUTHOR_IN_REDUX,
                        payload: {data: data.resData}
                    });
                    // console.log(data)

                }
            )
            .catch(err => console.log(err))
    }

}

export function getItemInfo(query) {

    return dispatch => {

        axios.post('/content/getItemInfo', query)
            .then(res => res.data)
            .then(data => {

                    // console.log(data.data);

                    dispatch({
                        type: PUT_ITEM_IN_REDUX,
                        payload: {data: data.data}
                    });

                }
            )
            .catch(err => console.log(err))
    }

}

export function setNewItemRate(query) {

    return dispatch => {

        axios.post('/content/setNewItemRate', query)
            .then(res => res.data)
            .then(data => {

                    // console.log(data);

                    dispatch({
                        type: RATE_ITEM_IN_REDUX,
                        payload: {data: data.newRate}
                    });

                }
            )
            .catch(err => console.log(err))
    }

}