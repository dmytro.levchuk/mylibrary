import React, {Component} from 'react';

import {connect} from 'react-redux';
import {Editor} from '@tinymce/tinymce-react';

import FormReduxAddNew from './FormReduxAddNew'

import {
    addNewContent
} from "../../../actions/content";

import "../Modals.scss"
import customRequiredInput from "../../atomic/customRequiredInput";
import {requiredInput, requiredInputRadioType} from "../../../validation";
import radio from "../../atomic/radio";
import users from "../../../reducers/users";

class AddNew extends Component {


    state = {
        type: 'audio',
        subType: 'music',
        title: '',
        description: '',
        imageUrl: '',
        source: '',
        content: '',

        errorStatus: false,

    };

    handleEditorChange = (content) => {

        // console.log(content)
        this.setState({content});
    }

    handleChangeType = (event) => {
        this.setState({
            type: event.target.value
        });
    }

    handleChangeSubType = (event) => {
        this.setState({
            subType: event.target.value
        });
    }

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSendForm = () => {

        // console.log(this.state)

        // type: 'audio',
        //     subType: 'music',
        //     title: '',
        //     description: '',
        //     imageUrl: '',
        //     source: '',
        //     content: '',

        if (this.state.type === 'audio' || this.state.type === 'video') {
            if (this.state.type && this.state.description && this.state.source) {
                this.props.addNewContent({
                    ...this.state,
                    authorName: this.props.login.loggedData.name
                });
            } else {
                this.setState({errorStatus: true});
            }


        } else {
            if (this.state.type && this.state.description && this.state.source && this.state.content ) {
                this.props.addNewContent({
                    ...this.state,
                    authorName: this.props.login.loggedData.name
                });
            } else {
                this.setState({errorStatus: true});
            }

        }



    }

    render() {

        let classText = (this.state.type === 'text') ? 'field-text' : 'd-none';
        let classTopImage = (this.state.type === 'text') ? 'field-wrapper field-wrapper_big' : 'd-none';
        let classMulti = (this.state.type !== 'text') ? 'field-wrapper field-wrapper_big' : 'd-none';

        return (
            <div className='modal__edit modal__wide'>
                <div className='modal__scroll'>
                    <div data-btn="btn-addnew-close"
                         className="modal__close"
                         onClick={() => document.body.style.overflow = "auto"}>
                        <div data-btn="btn-addnew-close" className="btn-close__line-top">
                        </div>
                        <div data-btn="btn-addnew-close" className="btn-close__line-bottom">
                        </div>
                    </div>

                    <h2 className='modal__title'>Add New Content</h2>

                    {/*<FormReduxAddNew onSubmit={this.handleSubmit}/>*/}
                    <div className='field-select field-select_row'>
                        <label className='field-select__name'>
                            Type
                        </label>
                        <div className="field-select__items">
                            <input name="type" type="radio" className="d-none" id="audio"
                                   value="audio"
                                   checked={this.state.type === "audio"}
                                   onChange={this.handleChangeType}/>
                            <div className="field-select__item">
                                <label htmlFor="audio">Audio</label>
                            </div>
                            <input name="type" type="radio" className="d-none" id="video"
                                   value="video"
                                   checked={this.state.type === "video"}
                                   onChange={this.handleChangeType}/>
                            <div className="field-select__item">
                                <label htmlFor="video">Video</label>
                            </div>
                            <input name="type" type="radio" className="d-none" id="text"
                                   value="text"
                                   checked={this.state.type === "text"}
                                   onChange={this.handleChangeType}/>
                            <div className="field-select__item">
                                <label htmlFor="text">Text</label>
                            </div>

                        </div>

                    </div>


                    <div className='field-select'>
                        <label className='field-select__name'>
                            Category
                        </label>
                        <div className="field-category__items">
                            <input name="subType" type="radio" className="d-none" id="music"
                                   value="music"
                                   checked={this.state.subType === "music"}
                                   onChange={this.handleChangeSubType}/>
                            <label className="field-category__item" htmlFor="music">
                                Music
                            </label>

                            <input name="subType" type="radio" className="d-none" id="sport"
                                   value="sport"
                                   checked={this.state.subType === "sport"}
                                   onChange={this.handleChangeSubType}/>
                            <label className="field-category__item" htmlFor="sport">
                                Sport
                            </label>
                            <input name="subType" type="radio" className="d-none" id="economic"
                                   value="economic"
                                   checked={this.state.subType === "economic"}
                                   onChange={this.handleChangeSubType}/>
                            <label className="field-category__item" htmlFor="economic">
                                Economic
                            </label>
                            <input name="subType" type="radio" className="d-none" id="art"
                                   value="art"
                                   checked={this.state.subType === "art"}
                                   onChange={this.handleChangeSubType}/>
                            <label className="field-category__item" htmlFor="art">
                                Art
                            </label>
                            <input name="subType" type="radio" className="d-none" id="nature"
                                   value="nature"
                                   checked={this.state.subType === "nature"}
                                   onChange={this.handleChangeSubType}/>
                            <label className="field-category__item" htmlFor="nature">
                                Nature
                            </label>

                        </div>
                    </div>


                    <div className='field-wrapper field-wrapper_big'>
                        <input name="title" type="text" placeholder="Title"
                               className="sign-form__input sign-form__input_big"
                               value={this.state.title}
                               onChange={this.handleInputChange}/>

                    </div>
                    <div className='field-wrapper field-wrapper_big'>
                        <input name="description" type="text" placeholder="Description"
                               className="sign-form__input sign-form__input_big"
                               value={this.state.description}
                               onChange={this.handleInputChange}/>

                    </div>

                    <div className={classTopImage}>
                        <input name="imageUrl" type="text" placeholder="Top Image (URL)"
                               className="sign-form__input sign-form__input_big"
                               value={this.state.imageUrl}
                               onChange={this.handleInputChange}/>

                    </div>

                    <div className={classMulti}>
                        <input name="source" type="text" placeholder="Youtube code or Soundcloud link)"
                               className="sign-form__input sign-form__input_big"
                               value={this.state.source}
                               onChange={this.handleInputChange}/>

                    </div>

                    <div className={classText}>

                        <Editor
                            init={{ width: "470px", height:"400px", margin: "0 auto", }}
                            value={this.state.content}
                            onEditorChange={this.handleEditorChange}
                        />
                    </div>
                    <div className='field-text text-align'>
                        {this.state.errorStatus && (
                            <div className='addnew-error'>
                                Please fill all fields
                            </div>
                        )
                        }


                        <button onClick={this.handleSendForm} name='signSbm' type="submit" className='sign-form__btn'>
                            Save
                        </button>
                    </div>
                </div>





            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        login: state.users,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addNewContent: (infoItem) => dispatch(addNewContent(infoItem)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddNew)