import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";

import users from "./users";
import modals from "./modals";
import content from "./content";

const rootReducer = combineReducers({
    users,
    modals,
    content,
    form: formReducer,
});

export default rootReducer;