import React, {Component} from 'react';

import {connect} from 'react-redux';

import {
    addNewUser,
} from "../../../actions/users";

import FormReduxRegistration from './FormReduxRegistration'


class RegistrationForm extends Component {

    handleSubmit = values => {
        let newForm = {
            name: values.name,
            email: values.email,
            password: values.password,
            password2: values.password2
        };

        this.props.addNewUser(newForm);
    };

    render () {

        let classExistEmail = (this.props.windowsStatus.existEmail) ? 'modal__error_msg' : 'd-none';

        return (
            <div className='modal__singin'>
                <div data-btn="btn-registration-close"
                     className="modal__close"
                     onClick={() => document.body.style.overflow = "auto"}>
                    <div data-btn="btn-registration-close" className="btn-close__line-top">
                    </div>
                    <div data-btn="btn-registration-close" className="btn-close__line-bottom">
                    </div>
                </div>
                <h2 className='modal__title'>Create New Account</h2>
                <div className={classExistEmail}>
                    This email is already used
                </div>
                <FormReduxRegistration onSubmit={this.handleSubmit}/>


            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        windowsStatus: state.modals.windowsStatus,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addNewUser: regForm => dispatch(addNewUser(regForm))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationForm)