export const correctEmail = input => {
    const chkEmail = new RegExp(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );

    return chkEmail.test(input)
        ? undefined
        : `You have to enter correct e-mail (example@com.com)`;
};

export const requiredInput = input => {
    return input ? undefined : `You have to fill this field`;
};

export const requiredInputRadioType = (input, allInputs) => {

    // console.log(allInputs)

    return (allInputs.type) ? undefined : `You have to fill this field`;
};

export const matchPasswordsReg = (input, allInputs) => {
    return input === allInputs.password
        ? undefined
        : "Your entered passwords isn't matched";
};

